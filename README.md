Fourth Tools
------------

Currently available tools:

* Calculator
* Timer
* Temporary file hosting

Dark mode is fully supported!

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.leestarb.fourthtools/)
