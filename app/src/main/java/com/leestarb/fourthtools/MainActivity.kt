package com.leestarb.fourthtools

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayout
import com.leestarb.fourthtools.components.calculator.Calculator
import com.leestarb.fourthtools.components.calculator.CalculatorViewModel
import com.leestarb.fourthtools.components.tempshare.TempShare
import com.leestarb.fourthtools.components.tempshare.TempShareViewModel
import com.leestarb.fourthtools.components.timer.Timer
import com.leestarb.fourthtools.components.timer.TimerViewModel
import com.leestarb.fourthtools.databinding.ActivityMainBinding
import com.leestarb.fourthtools.databinding.DialogAboutAppBinding
import com.mikepenz.aboutlibraries.LibsBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var calculatorViewModel: CalculatorViewModel
    private lateinit var timerViewModel: TimerViewModel
    private lateinit var tempShareViewModel: TempShareViewModel
    val okHttpClient = OkHttpClient.Builder().apply {
        addInterceptor(HttpLoggingInterceptor().apply {
            setLevel(HttpLoggingInterceptor.Level.HEADERS)
            setLevel(HttpLoggingInterceptor.Level.BODY)
        })
    }.build()
    private val currentTab: Fragment? get() = supportFragmentManager.findFragmentByTag("currentTab")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setSupportActionBar(binding.mainToolbar)
        setContentView(binding.root)

        calculatorViewModel = ViewModelProvider(this)[CalculatorViewModel::class.java]
        timerViewModel = ViewModelProvider(this)[TimerViewModel::class.java]
        tempShareViewModel = ViewModelProvider(this)[TempShareViewModel::class.java]
        initUI()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.menu_about -> {
                Dialog(this).apply {
                    setCancelable(false)
                    window!!.setBackgroundDrawableResource(R.drawable.dialog_rounded)

                    val dialogAboutAppBinding = DialogAboutAppBinding.inflate(layoutInflater)
                    setContentView(dialogAboutAppBinding.root)
                    dialogAboutAppBinding.aboutClose.setOnClickListener {
                        dismiss()
                    }
                    dialogAboutAppBinding.aboutSettings.setOnClickListener {
                        dismiss()
                        startActivity(Intent(applicationContext, SettingsActivity::class.java))
                    }
                    dialogAboutAppBinding.aboutAcknowledgements.setOnClickListener {
                        dismiss()
                        LibsBuilder().start(applicationContext)
                    }
                    @Suppress("DEPRECATION")
                    dialogAboutAppBinding.aboutVersion.append(packageManager.getPackageInfo(packageName, 0).versionName)
                }.show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initUI() {
        binding.tabsLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.contentDescription) {
                    "Calculator" -> switchTabFragment(Calculator::class.java.newInstance())
                    "Timer" -> switchTabFragment(Timer::class.java.newInstance())
                    "TempShare" -> switchTabFragment(TempShare::class.java.newInstance())
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {
                if (currentTab == null) onTabSelected(tab)
            }
        })
        binding.tabsLayout.selectTab(binding.tabsLayout.getTabAt(0))
    }

    private fun switchTabFragment(fragment: Fragment) {
        supportFragmentManager.commit {
            replace(R.id.fragmentsContainer, fragment, "currentTab")
        }
    }

    @Suppress("unused")
    fun onCalculatorBtnClick(view: View) {
        (currentTab as Calculator).onBtnClick(view)
    }
    @Suppress("unused", "unused_parameter")
    fun onCalculatorClearClick(view: View) {
        (currentTab as Calculator).onClearClick()
    }
}