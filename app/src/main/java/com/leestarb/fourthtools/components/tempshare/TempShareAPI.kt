package com.leestarb.fourthtools.components.tempshare

import io.reactivex.rxjava3.core.Single
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.POST

interface TempShareAPI {
    @POST("upload")
    fun upload(
        @Body file: RequestBody
    ) : Single<TempShareResponse>
}