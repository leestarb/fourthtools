package com.leestarb.fourthtools.components.timer

import android.annotation.SuppressLint
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.os.PowerManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintSet
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.leestarb.fourthtools.R
import com.leestarb.fourthtools.databinding.FragmentTimerBinding
import org.apache.commons.lang3.StringUtils
import java.io.FileOutputStream
import java.util.concurrent.TimeUnit
import kotlin.math.roundToLong

class Timer : Fragment() {
    private lateinit var binding: FragmentTimerBinding
    private lateinit var timerServiceIntent: Intent
    private lateinit var wakeLock: PowerManager.WakeLock
    private lateinit var viewModel: TimerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity())[TimerViewModel::class.java]

        val powerManager = requireContext().getSystemService(Context.POWER_SERVICE) as PowerManager
        @Suppress("DEPRECATION")
        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "${getString(R.string.app_name)}:${javaClass.name}")
        timerServiceIntent = Intent(requireContext(), TimerService::class.java)
    }

    override fun onResume() {
        super.onResume()
        wakeLock.acquire(360000000)  // 100 hours
        fullscreenChecksRV(false)
        if (binding.timeText.text.isNullOrBlank()) binding.timeText.text = getTimeString(0.0)
        if (viewModel.isRunning.value!!) requireContext().registerReceiver(updateTime, IntentFilter(TimerService.TIMER_UPDATED))
    }

    override fun onPause() {
        super.onPause()
        if (viewModel.isRunning.value!!) requireContext().unregisterReceiver(updateTime)
        wakeLock.release()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTimerBinding.inflate(inflater, container, false)
        initUI()
        if (viewModel.isRunning.value!!) runPauseTimer(true)
        return binding.root
    }

    private fun initUI() {
        if (viewModel.time.value!! > 0.0) binding.timeText.text = getTimeString(viewModel.time.value!!)
        binding.startStopBtn.setOnClickListener { runPauseTimer() }
        binding.checkBtn.setOnClickListener { checkTimer() }
        binding.resetBtn.setOnClickListener { resetTimer() }
        binding.exportBtn.setOnClickListener { exportChecks() }
        binding.clearBtn.setOnClickListener { clearChecks() }

        binding.timeChecksRV.layoutManager = LinearLayoutManager(context)
        binding.timeChecksRV.adapter = TimeCheckAdapter(viewModel.timeChecksList.value!!)
        binding.timeChecksRV.setHasFixedSize(true)
    }

    private val updateTime = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val time = intent.getDoubleExtra(TimerService.TIME_EXTRA, 0.0)
            binding.timeText.text = getTimeString(time)
//            TO PREVENT LAGS
            if (time % 100 == 0.0) viewModel.time.value = time
        }
    }

    @SuppressLint("SetTextI18n")
    private fun runPauseTimer(isResumed: Boolean = false) {
        if (isResumed || !viewModel.isRunning.value!!) {
            requireContext().registerReceiver(updateTime, IntentFilter(TimerService.TIMER_UPDATED))
            timerServiceIntent.putExtra(TimerService.TIME_EXTRA, viewModel.time.value)
            if (!isResumed) requireContext().startService(timerServiceIntent)
            binding.startStopBtn.icon = AppCompatResources.getDrawable(requireContext(), R.drawable.ic_baseline_pause_24)
            binding.startStopBtn.text = "PAUSE"
            viewModel.isRunning.value = true
        } else {
            requireContext().unregisterReceiver(updateTime)
            requireContext().stopService(timerServiceIntent)
            binding.startStopBtn.icon = AppCompatResources.getDrawable(requireContext(), R.drawable.ic_baseline_play_arrow_24)
            binding.startStopBtn.text = "RUN"
            viewModel.isRunning.value = false
        }
    }

    private fun checkTimer() {
        val adapter = binding.timeChecksRV.adapter as TimeCheckAdapter
        adapter.checks.add(TimeCheck(adapter.itemCount + 1, binding.timeText.text.toString()))
        adapter.notifyItemInserted(adapter.checks.lastIndex)
    }

    private fun resetTimer() {
        if (viewModel.time.value == 0.0) return

        MaterialAlertDialogBuilder(requireContext()).apply {
            setTitle("Timer reset")
            setMessage("Are you sure you want to reset the timer?")
            setNegativeButton("No", null)
            setPositiveButton("Yes") { _, _ ->
                if (viewModel.isRunning.value!!) runPauseTimer()
                viewModel.time.value = 0.0
                binding.timeText.text = getTimeString(viewModel.time.value!!)
            }
        }.show()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun clearChecks() {
        if (binding.timeChecksRV.adapter!!.itemCount <= 0) return

        MaterialAlertDialogBuilder(requireContext()).apply {
            setTitle("Clear all the checks")
            setMessage("Are you sure you want to clear all the checks?")
            setNegativeButton("No", null)
            setPositiveButton("Yes") { _, _ ->
                val adapter = binding.timeChecksRV.adapter as TimeCheckAdapter
                adapter.checks.clear()
                adapter.notifyDataSetChanged()
            }
        }.show()
    }

    private fun exportChecks() {
        if (binding.timeChecksRV.adapter!!.itemCount <= 0) return Toast.makeText(context, "No checks to export", Toast.LENGTH_SHORT).show()
        if (viewModel.isRunning.value!!) runPauseTimer()

        var choosenMethod: String? = null
        val methods = arrayOf(EXPORT_IMAGE, EXPORT_FILE)
        MaterialAlertDialogBuilder(requireContext()).apply {
            setTitle("Choose checks export type")
            setSingleChoiceItems(methods, -1) { _, which -> choosenMethod = methods[which] }
            setNegativeButton("Cancel") { _, _ -> choosenMethod = null }
            setPositiveButton("Ok") { _, _ ->
                @Suppress("DEPRECATION")
                when (choosenMethod) {
                    EXPORT_IMAGE -> {
                        fullscreenChecksRV(true)
                        val saveImageIntent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
                            addCategory(Intent.CATEGORY_OPENABLE)
                            type = "*/*"
                            putExtra(Intent.EXTRA_TITLE, "checks-image_${System.currentTimeMillis()}.jpg")
                        }
                        startActivityForResult(saveImageIntent, RESULT_EXPORT_IMAGE)
                    }

                    EXPORT_FILE -> {
                        val saveFileIntent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
                            addCategory(Intent.CATEGORY_OPENABLE)
                            type = "*/*"
                            putExtra(Intent.EXTRA_TITLE, "checks-file_${System.currentTimeMillis()}.txt")
                        }
                        startActivityForResult(saveFileIntent, RESULT_EXPORT_FILE)
                    }
                }
                choosenMethod = null
            }
        }.show()
    }

    @Deprecated("Deprecated in Java", ReplaceWith("super.onActivityResult(requestCode, resultCode, data)"))
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        @Suppress("DEPRECATION")
        super.onActivityResult(requestCode, resultCode, data)
        if ((resultCode != Activity.RESULT_OK) || (data == null) || (data.data == null)) return
        when (requestCode) {
            RESULT_EXPORT_IMAGE -> {
                val watermark = requireActivity().findViewById<TextView>(R.id.watermarkText)
                val bitmap = Bitmap.createBitmap(binding.timerLayoutRoot.width, binding.timerLayoutRoot.height + watermark.height, Bitmap.Config.ARGB_8888)
                val canvas = Canvas(bitmap)
                binding.timerLayoutRoot.draw(canvas)
                watermark.draw(canvas)

                data.data!!.also { uri ->
                    requireContext().contentResolver.openFileDescriptor(uri, "w")!!.use {
                        FileOutputStream(it.fileDescriptor).use { fos ->
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos)
                        }
                    }
                }
                fullscreenChecksRV(false)
                Toast.makeText(context, "Export image successful", Toast.LENGTH_SHORT).show()
            }

            RESULT_EXPORT_FILE -> {
                data.data!!.also { uri ->
                    requireContext().contentResolver.openFileDescriptor(uri, "w")!!.use {
                        FileOutputStream(it.fileDescriptor).use { fos ->
                            var text = "# Recorded with Timer!! by leestarb" + System.lineSeparator() + System.lineSeparator()
                            text += "Current time: ${binding.timeText.text}" + System.lineSeparator() + System.lineSeparator()
                            for (timeCheck in (binding.timeChecksRV.adapter as TimeCheckAdapter).checks) {
                                text += "- ${timeCheck.number}. ${timeCheck.time}"
                                if (!timeCheck.note.isNullOrBlank()) {
                                    text += System.lineSeparator() + "\"${timeCheck.note}\""
                                }
                                text += System.lineSeparator() + System.lineSeparator()
                            }
                            fos.write(text.toByteArray())
                        }
                    }
                }
                Toast.makeText(context, "Export file successful", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun fullscreenChecksRV(enable: Boolean) {
        val constraintSet = ConstraintSet()
        if (enable) {
            binding.buttons.visibility = View.GONE
            constraintSet.clone(binding.timerLayoutRoot)
            constraintSet.connect(R.id.timeChecksRV, ConstraintSet.TOP, ConstraintSet.UNSET, ConstraintSet.BOTTOM)
            constraintSet.connect(R.id.timeChecksRV, ConstraintSet.TOP, R.id.timeText, ConstraintSet.BOTTOM)
            constraintSet.applyTo(binding.timerLayoutRoot)
        } else if (binding.buttons.visibility == View.GONE) {
            binding.buttons.visibility = View.VISIBLE
            constraintSet.clone(binding.timerLayoutRoot)
            constraintSet.connect(R.id.timeChecksRV, ConstraintSet.TOP, ConstraintSet.UNSET, ConstraintSet.BOTTOM)
            constraintSet.connect(R.id.timeChecksRV, ConstraintSet.TOP, R.id.buttons, ConstraintSet.BOTTOM)
            constraintSet.applyTo(binding.timerLayoutRoot)
        }
    }

    private fun getTimeString(time: Double): String {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext())
        val millsPrecision = when(sharedPreferences.getBoolean("timer_extended_precision_counter", false)) {
            true -> 3
            false -> 2
        }

        val mills = time.roundToLong()
        val tu = TimeUnit.MILLISECONDS
        val h = tu.toHours(mills) % 100
        val m = tu.toMinutes(mills) % 60
        val s = tu.toSeconds(mills) % 60
        return if (h > 0) {
            "%02d:%02d:%02d:%02d".format(h, m, s,  StringUtils.left(((if (mills < 1000) mills else(mills % 1000)).toString()), millsPrecision).toInt())
        } else {
            "%02d:%02d:%02d".format(m, s,  StringUtils.left((if (mills < 1000) mills else(mills % 1000)).toString() + "00", millsPrecision).toInt())
        }
    }

    companion object {
        const val EXPORT_IMAGE = "Image file"
        const val EXPORT_FILE = "Text file"
        const val RESULT_EXPORT_IMAGE = 15175
        const val RESULT_EXPORT_FILE = 6145
    }
}