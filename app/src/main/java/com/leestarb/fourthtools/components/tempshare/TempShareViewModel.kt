package com.leestarb.fourthtools.components.tempshare

import android.app.Application
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class TempShareViewModel(application: Application) : AndroidViewModel(application) {
    val filePath = MutableLiveData<String>()
    val fileUri = MutableLiveData<Uri>()
    val fileSize = MutableLiveData<Int>()
    val fileID = MutableLiveData<String>()
    val filePassword = MutableLiveData<String>()
    val fileDeletePassword = MutableLiveData<String>()
    val fileDownloadLink = MutableLiveData<String>()
}