package com.leestarb.fourthtools.components.tempshare

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.leestarb.fourthtools.MainActivity
import com.leestarb.fourthtools.R
import com.leestarb.fourthtools.databinding.FragmentTempShareBinding
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.observers.DisposableSingleObserver
import io.reactivex.rxjava3.schedulers.Schedulers
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.apache.commons.io.FileUtils
import org.apache.commons.validator.routines.UrlValidator
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.FileInputStream

class TempShare : Fragment() {
    private lateinit var binding: FragmentTempShareBinding
    private lateinit var tempShareAPI: TempShareAPI
    private lateinit var viewModel: TempShareViewModel
    private lateinit var baseAPIURL: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity())[TempShareViewModel::class.java]

        val preferenceManager = PreferenceManager.getDefaultSharedPreferences(requireContext())
        val customURL = preferenceManager.getString("temp_share_custom_url", null)
        baseAPIURL = if (
            preferenceManager.getBoolean("temp_share_use_custom_url", false) && !customURL.isNullOrBlank() && UrlValidator.getInstance().isValid(customURL)
        ) customURL else TEMP_SHARE_DEFAULT_URL
        if (!baseAPIURL.endsWith("/")) baseAPIURL += "/"

        tempShareAPI = Retrofit.Builder().apply {
            baseUrl(baseAPIURL)
            client((requireActivity() as MainActivity).okHttpClient)
            addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            addConverterFactory(GsonConverterFactory.create())
        }.build().create(TempShareAPI::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTempShareBinding.inflate(inflater, container, false)
        initUI()
        Toast.makeText(context, "URL in use: $baseAPIURL", Toast.LENGTH_LONG).show()
        return binding.root
    }

    private fun initUI() {
        viewModel.filePath.observe(viewLifecycleOwner) {
            if (it.isNullOrBlank()) return@observe
            binding.tempShareLocalFileInfo.visibility = View.VISIBLE
            binding.attachedFilePathText.text = it
        }
        viewModel.fileSize.observe(viewLifecycleOwner) {
            if (it == null) return@observe
            binding.tempShareLocalFileInfo.visibility = View.VISIBLE
            binding.attachedFileSizeText.text = FileUtils.byteCountToDisplaySize(it.toBigInteger())
        }
        viewModel.fileID.observe(viewLifecycleOwner) {
            if (it.isNullOrBlank()) return@observe
            binding.tempShareServerFileInfo.visibility = View.VISIBLE
            binding.serverFileIDText.text = it
        }
        viewModel.filePassword.observe(viewLifecycleOwner) {
            if (it.isNullOrBlank()) return@observe
            binding.tempShareServerFileInfo.visibility = View.VISIBLE
            binding.serverFilePasswordText.text = it
        }
        viewModel.fileDeletePassword.observe(viewLifecycleOwner) {
            if (it.isNullOrBlank()) return@observe
            binding.tempShareServerFileInfo.visibility = View.VISIBLE
            binding.serverFileDeletePasswordText.text = it
        }
        viewModel.fileDownloadLink.observe(viewLifecycleOwner) {
            if (it.isNullOrBlank()) return@observe
            binding.tempShareServerFileInfo.visibility = View.VISIBLE
            binding.serverFileDownloadLinkText.text = it
        }

        binding.selectFileBtn.setOnClickListener {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = "*/*"
                action = Intent.ACTION_GET_CONTENT
                putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false)
            }
            @Suppress("DEPRECATION")
            startActivityForResult(intent, RESULT_FILE_URI)
        }
        binding.uploadFileBtn.setOnClickListener { startUploading() }

        if (baseAPIURL.contains("tempfiles.ninja", true) && viewModel.filePath.value.isNullOrBlank()) {
            MaterialAlertDialogBuilder(requireContext()).apply {
                setTitle("Disclaimer")
                setMessage(getString(R.string.temp_share_disclaimer, TEMP_SHARE_DEFAULT_URL))
                setPositiveButton("OK", null)
                setCancelable(false)
            }.show()
        }
    }

    @Deprecated("Deprecated in Java", ReplaceWith("super.onActivityResult(requestCode, resultCode, data)"))
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        @Suppress("DEPRECATION")
        super.onActivityResult(requestCode, resultCode, data)
        if ((resultCode != Activity.RESULT_OK) || (requestCode != RESULT_FILE_URI) || (data == null) || (data.data == null)) return
        requireContext().contentResolver.openFileDescriptor(data.data!!, "r")!!.use {
            FileInputStream(it.fileDescriptor).use { fis ->
                val fileBytesSize = fis.readBytes().size
                if (fileBytesSize >= 104857600) {
                    Toast.makeText(context, "Too large file!", Toast.LENGTH_LONG).show()
                    return
                }
                viewModel.fileSize.value = fileBytesSize
            }
        }
        binding.tempShareServerFileInfo.visibility = View.GONE
        viewModel.filePath.value = Uri.decode(data.data.toString())
        viewModel.fileUri.value = data.data
    }

    private fun startUploading() {
        if (viewModel.fileUri.value == null) {
            Toast.makeText(context, "No file to upload", Toast.LENGTH_SHORT).show()
            return
        }

        var fileContent: ByteArray
        requireContext().contentResolver.openFileDescriptor(viewModel.fileUri.value!!, "r")!!.use {
            FileInputStream(it.fileDescriptor).use { fis ->
                fileContent = fis.readBytes()
            }
        }
        tempShareAPI.upload(fileContent.toRequestBody(MultipartBody.FORM))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableSingleObserver<TempShareResponse>() {
                override fun onSuccess(t: TempShareResponse) {
                    Log.i("onSuccess", t.toString())
                    viewModel.fileID.value = t.id
                    viewModel.filePassword.value = t.password
                    viewModel.fileDeletePassword.value = t.deletePassword
                    viewModel.fileDownloadLink.value = t.downloadUrl
                    Toast.makeText(context, "Uploading completed!", Toast.LENGTH_LONG).show()
                }

                override fun onError(e: Throwable) {
                    Log.e("onError", e.stackTraceToString())
                    Toast.makeText(context, "Uploading failed!", Toast.LENGTH_LONG).show()
                }
            })
    }

    companion object {
        const val TEMP_SHARE_DEFAULT_URL = "https://tempfiles.ninja/api"
        const val RESULT_FILE_URI = 6550
    }
}