package com.leestarb.fourthtools.components.timer

import android.app.Service
import android.content.Intent
import android.os.IBinder
import java.util.Timer
import java.util.TimerTask

class TimerService : Service() {
    private lateinit var timer: Timer
    override fun onBind(intent: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val time = intent.getDoubleExtra(TIME_EXTRA, 0.0)
        timer = Timer()
        timer.scheduleAtFixedRate(TimeTask(time), 0, 1)
        return START_NOT_STICKY
    }

    override fun onDestroy() {
        timer.cancel()
        super.onDestroy()
    }

    private inner class TimeTask(private var time: Double) : TimerTask() {
        override fun run() {
            val timeUpdateIntent = Intent(TIMER_UPDATED)
            time++
            timeUpdateIntent.putExtra(TIME_EXTRA, time)
            sendBroadcast(timeUpdateIntent)
        }
    }

    companion object {
        const val TIMER_UPDATED = "timerUpdated"
        const val TIME_EXTRA = "timeExtra"
    }
}