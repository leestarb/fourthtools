package com.leestarb.fourthtools.components.timer

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.leestarb.fourthtools.R
import com.leestarb.fourthtools.databinding.TimeCheckRvBinding

class TimeCheckAdapter(
    val checks: MutableList<TimeCheck>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var binding: TimeCheckRvBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = TimeCheckRvBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TimeCheckViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        (holder as TimeCheckViewHolder).initUI(checks[position], position)
    }

    override fun getItemCount() = checks.size

    private inner class TimeCheckViewHolder(binding: TimeCheckRvBinding) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
        fun initUI(timeCheck: TimeCheck, position: Int) {
            binding.checkNumber.text = "${timeCheck.number}."
            binding.checkTimeText.text = timeCheck.time
            val noteEdit = binding.timeCheckNoteEdit
            if (timeCheck.note != null) noteEdit.setText(timeCheck.note)
            noteEdit.doOnTextChanged { text, _, _, _ -> timeCheck.note = text.toString() }

            binding.removeTimeCheck.setOnClickListener { _ ->
                val checksBackup = checks.toList()
//                TODO: FIND A BETTER IMPLEMENTATION OF ITEM REMOVING
                checks.removeAt(position)
                notifyItemRemoved(position)

                val dataSaveList = mutableListOf<Pair<String, String?>>()
                checks.forEachIndexed { _, tc ->
                    dataSaveList.add(Pair(tc.time, tc.note))
                }
                checks.clear()
                dataSaveList.forEach {
                    checks.add(TimeCheck(itemCount + 1, it.first, it.second))
                }
                notifyDataSetChanged()

                Snackbar.make(binding.root, "Check ${timeCheck.number} has been removed", Snackbar.LENGTH_LONG).apply {
                    setAction("RESTORE") {
                        checks.clear()
                        checks.addAll(checksBackup)
                        notifyDataSetChanged()
                    }
                }.show()
            }

            binding.expandTimeCheckNote.setOnClickListener { it as ImageView
                if (noteEdit.maxLines == 1) {
                    noteEdit.maxLines = 999
                    it.setImageResource(R.drawable.ic_baseline_keyboard_arrow_up_24)
                } else {
                    noteEdit.maxLines = 1
                    it.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24)
                }
            }
        }
    }
}