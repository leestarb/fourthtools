package com.leestarb.fourthtools.components.timer

data class TimeCheck(
    var number: Int,
    val time: String,
    var note: String? = null,
)
