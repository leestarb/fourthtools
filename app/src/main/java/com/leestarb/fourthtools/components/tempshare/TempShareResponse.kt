package com.leestarb.fourthtools.components.tempshare

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class TempShareResponse(
    val id: String? = null,
    val password: String? = null,
    @SerializedName("delete_password") val deletePassword: String? = null,
    @SerializedName("download_url") val downloadUrl: String? = null,
) : Parcelable
