package com.leestarb.fourthtools.components.calculator

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.leestarb.fourthtools.databinding.FragmentCalculatorBinding
import net.objecthunter.exp4j.ExpressionBuilder

class Calculator : Fragment() {
    private lateinit var binding: FragmentCalculatorBinding
    private lateinit var viewModel: CalculatorViewModel
    private var clearClicked = false
    get() {
        if (PreferenceManager.getDefaultSharedPreferences(requireContext()).getBoolean("calc_double_click_to_clear", true))
            return field
        else
            return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity())[CalculatorViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCalculatorBinding.inflate(inflater, container, false)
        binding.CalculatorResultText.showSoftInputOnFocus = false
        binding.CalculatorResultText.movementMethod = ScrollingMovementMethod.getInstance()
        binding.CalculatorResultText.text = viewModel.expressionText.value.toString()
        binding.CalculatorResultText.doOnTextChanged { text, _, _, _ -> viewModel.expressionText.value = text.toString() }
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    fun onBtnClick(btn: View) {
        clearClicked = false
        binding.CalculatorResultText.text = binding.CalculatorResultText.text.toString().replace("Infinity", "0")
        when ((btn as Button).text) {
            "R" -> del()
            "()" -> {
                if (binding.CalculatorResultText.text.toString() == "0") {
                    binding.CalculatorResultText.text = ""
                }
                if (binding.CalculatorResultText.text.toString().count { it == '(' } <= binding.CalculatorResultText.text.toString().count { it == ')' }) {
                    binding.CalculatorResultText.append("(")
                } else if (binding.CalculatorResultText.text[binding.CalculatorResultText.text.lastIndex].toString() != "(") {
                    binding.CalculatorResultText.append(")")
                }
            }
            "=" -> {
                while (binding.CalculatorResultText.text[binding.CalculatorResultText.text.lastIndex].toString() in OP_SYM) del()
                try {
                    var result = ExpressionBuilder(binding.CalculatorResultText.text.toString()).build().evaluate().toString()
                    if (result.endsWith(".0")) result = result.removeSuffix(".0")
                    binding.CalculatorResultText.text = result
                } catch (exception: java.lang.RuntimeException) {
                    when(exception) {
                        is java.lang.IllegalArgumentException, is java.lang.ArithmeticException ->
                            binding.CalculatorResultText.text = "Infinity"
                        else -> throw exception
                    }
                } finally {
                    scrollCalculatorResultTextToTop()
                }
            }
            else -> {
                if (btn.text in OP_SYM.replace('-', Char.MIN_VALUE)) {
                    if (binding.CalculatorResultText.text.toString() == "0") return
                    else if (binding.CalculatorResultText.text[binding.CalculatorResultText.text.lastIndex].toString() in OP_SYM) {
                        del()
                    }
                    binding.CalculatorResultText.append(btn.text)
                    return
                }
                if (binding.CalculatorResultText.text.toString() == "0") {
                    binding.CalculatorResultText.text = ""
                }
                binding.CalculatorResultText.append(btn.text)
            }
        }
    }

    fun onClearClick() {
        if (clearClicked) {
            binding.CalculatorResultText.text = "0"
            scrollCalculatorResultTextToTop()
            clearClicked = false
        } else clearClicked = true
    }

    private fun del() {
        if (binding.CalculatorResultText.text.toString() != "0") {
            binding.CalculatorResultText.text = binding.CalculatorResultText.text.removeSuffix(binding.CalculatorResultText.text[binding.CalculatorResultText.text.lastIndex].toString())
            if (binding.CalculatorResultText.text.isBlank()) {
                binding.CalculatorResultText.text = "0"
            }
        }
    }

    private fun scrollCalculatorResultTextToTop() {
        try {
            val scrollAmount = binding.CalculatorResultText.layout.getLineTop(binding.CalculatorResultText.lineCount) - binding.CalculatorResultText.height
            if (scrollAmount > 0)
                binding.CalculatorResultText.scrollTo(0, scrollAmount)
            else
                binding.CalculatorResultText.scrollTo(0, 0)
        } catch (_: NullPointerException) {}  // OK?
    }

    companion object {
        const val OP_SYM = "/*+-^.("
    }
}