package com.leestarb.fourthtools.components.timer

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class TimerViewModel(application: Application) : AndroidViewModel(application) {
    val time = MutableLiveData(0.0)
    val isRunning = MutableLiveData(false)
    val timeChecksList = MutableLiveData(mutableListOf<TimeCheck>())
}