package com.leestarb.fourthtools.components.calculator

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class CalculatorViewModel(application: Application) : AndroidViewModel(application) {
    val expressionText = MutableLiveData("0")
}